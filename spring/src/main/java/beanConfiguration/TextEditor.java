package beanConfiguration;

/**
 * Created by vhphat on 12/22/2014.
 */
public class TextEditor {
    private SpellChecker spellChecker;

    public TextEditor(SpellChecker spellChecker) {
        this.spellChecker = spellChecker;
    }

    public boolean spellCheck(String text) {
        return spellChecker.checkSpelling(text);
    }
}
