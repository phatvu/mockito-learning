package beanConfiguration;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.when;

/**
 * Created by vhphat on 12/22/2014.
 */
@RunWith(MockitoJUnitRunner.class)
public class TextEditorTest {

    @InjectMocks
    private TextEditor testingObject;

    @Mock
    SpellChecker spellChecker;

    @Test
    public void testSpellCheck() throws Exception {
        when(spellChecker.checkSpelling("A")).thenReturn(true);
        assertEquals(testingObject.spellCheck("A"), true);
    }
}
