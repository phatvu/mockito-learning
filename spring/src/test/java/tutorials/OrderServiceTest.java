package tutorials;

/**
 * Created by vhphat on 12/19/2014.
 */

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class OrderServiceTest {
    private static final int TEST_ORDER_ID = 15;
    private static final int TEST_SHOES_PRICE = 2;
    private static final int TEST_SHIRT_PRICE = 1;

    // Instantiates testing object instance and tries to inject fields annotated with
    // @Mock or @Spy into private fields of testing object
    @InjectMocks
    private OrderService testingObject;

    @Spy
    private PriceService priceService;

    // without this, it will lead to NPE.
    @Mock
    private OrderDao orderDao;

    @Before
    public void initMocks(){
        // must be the below code if not use : @RunWith(MockitoJUnitRunner.class)
       // MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetOrderService(){
        Order order = new Order(Arrays.asList(Item.SHOES, Item.SHIRT));
        when(orderDao.getOrder(TEST_ORDER_ID)).thenReturn(order);

        //notice different Mockito syntax for spy
        doReturn(TEST_SHIRT_PRICE).when(priceService).getActualPrice(Item.SHIRT);
        doReturn(TEST_SHOES_PRICE).when(priceService).getActualPrice(Item.SHOES);


        //call testing method
        int actualOrderPrice = testingObject.getOrderPrice(TEST_ORDER_ID);

        verify(orderDao, times(1)).getOrder(TEST_ORDER_ID);
        assertEquals(TEST_SHIRT_PRICE + TEST_SHOES_PRICE, actualOrderPrice);
    }
}