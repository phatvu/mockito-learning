package demo;

/**
 * Created by vhphat on 12/25/2014.
 */
public class Person {
    private final Integer personID;
    private final String personName;

    public Person(Integer personID, String personName) {
        this.personID = personID;
        this.personName = personName;
    }

    public Integer getPersonID() {
        return personID;
    }

    public String getPersonName() {
        return personName;
    }
}