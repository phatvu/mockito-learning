package tutorials;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import java.util.LinkedList;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNull;
import static org.mockito.Mockito.*;

/**
 * Created by vhphat on 12/17/2014.
 */
@RunWith(MockitoJUnitRunner.class)
public class Main {
    // 1. Let's verify some behaviour!
    @Test
    public void verifySomeBehaviour() throws Exception{
        //mock creation
        List mockedList = mock(List.class);

        //using mock object
        mockedList.add("one");
        mockedList.clear();

        //verification
        verify(mockedList).add("one");
        verify(mockedList).clear();

        verify(mockedList, never()).get(1); // never called.
    }

    // 2. How about some stubbing?
    @Test
    public void someStubbing() throws Exception{
        //You can mock concrete classes, not only interfaces
        LinkedList mockedList = mock(LinkedList.class);

        //stubbing
        when(mockedList.get(0)).thenReturn("first");
        when(mockedList.get(1)).thenThrow(new RuntimeException());

        assertEquals("first", mockedList.get(0));

        //following throws runtime exception
        //System.out.println(mockedList.get(1));

        //"null" because get(999) was not stubbed
        assertNull(mockedList.get(999));
    }

    // 3. Argument matchers
    @Test
    public void argumentMatchers() throws Exception{
        LinkedList mockedList = mock(LinkedList.class);
        //stubbing using built-in anyInt() argument matcher
        when(mockedList.get(anyInt())).thenReturn("element");

        assertEquals("element", mockedList.get(999));

        //you can also verify using an argument matcher
        verify(mockedList).get(anyInt());

        // If you are using argument matchers, all arguments have to be provided by matchers.
        when(mockedList.subList(anyInt(), anyInt())).thenReturn(null);
        assertNull(mockedList.subList(2, 1));

        //TODO : custom argument matchers / hamcrest matchers.
    }

    // Verifying exact number of invocations / at least x / never
    @Test
    public void verifyExactNumberOfInvocations() throws Exception{
        LinkedList mockedList = mock(LinkedList.class);
        //using mock
        mockedList.add("once");

        mockedList.add("twice");
        mockedList.add("twice");

        mockedList.add("three times");
        mockedList.add("three times");
        mockedList.add("three times");

        //following two verifications work exactly the same - times(1) is used by default
        verify(mockedList).add("once");
        verify(mockedList, times(1)).add("once");

        //exact number of invocations verification
        verify(mockedList, times(2)).add("twice");
        verify(mockedList, times(3)).add("three times");

        //verification using never(). never() is an alias to times(0)
        verify(mockedList, never()).add("never happened");

        //verification using atLeast()/atMost()
        verify(mockedList, atLeastOnce()).add("three times");
        verify(mockedList, atLeast(3)).add("three times");
    }

    // 5. Stubbing void methods with exceptions
    @Test(expected = RuntimeException.class)
    public void stubbingVoidMethodsWithExceptions() throws Exception{
        LinkedList mockedList = mock(LinkedList.class);
        doThrow(new RuntimeException()).when(mockedList).clear();

        //following throws RuntimeException:
        mockedList.clear();
    }

    // 6. Verification in order
    @Test
    public void orderVerification() throws Exception{
        // A. Single mock whose methods must be invoked in a particular order
        List singleMock = mock(List.class);

        //using a single mock
        singleMock.add("was added first");
        singleMock.add("was added second");

        //create an inOrder verifier for a single mock
        InOrder inOrder = inOrder(singleMock);

        //following will make sure that add is first called with "was added first, then with "was added second"
        inOrder.verify(singleMock).add("was added first");
        inOrder.verify(singleMock).add("was added second");

        // B. Multiple mocks that must be used in a particular order
        List firstMock = mock(List.class);
        List secondMock = mock(List.class);

        //using mocks
        firstMock.add("was called first");
        firstMock.add("was called first-2");
        secondMock.add("was called second");

        //create inOrder object passing any mocks that need to be verified in order
        InOrder inOrder2 = inOrder(firstMock, secondMock);

        //following will make sure that firstMock was called before secondMock
        inOrder2.verify(firstMock).add("was called first");
        inOrder2.verify(firstMock).add("was called first-2");
        inOrder2.verify(secondMock).add("was called second");
    }

    // 7. Making sure interaction(s) never happened on mock
    @Test
    public void interactionNeverHappenedOnMock() throws Exception{
        List mockOne = mock(List.class);
        List mockTwo = mock(List.class);
        List mockThree = mock(List.class);
        //using mocks - only mockOne is interacted
        mockOne.add("one");

        //ordinary verification
        verify(mockOne).add("one");

        //verify that method was never called on a mock
        verify(mockOne, never()).add("two");

        //verify that other mocks were not interacted
        verifyZeroInteractions(mockTwo, mockThree);

        // failed.
        /*mockOne.add("one");
        verifyZeroInteractions(mockOne);*/
    }

    // 8. Finding redundant invocations
    @Test
    public void redundantInvocations() throws Exception {
        List mockedList = mock(List.class);
        //using mocks
        mockedList.add("one");
        mockedList.add("two");

        verify(mockedList).add("one");

        // following verification will fail
        // removing add "two"
        verifyNoMoreInteractions(mockedList);
    }

    // 10. Stubbing consecutive calls (iterator-style stubbing)
    @Test
    public void consecutiveCalls() throws Exception {
        List mockedList = mock(List.class);
        when(mockedList.get(anyInt())).thenReturn("one", "two");
        assertEquals("one", mockedList.get(0));
        assertEquals("two", mockedList.get(0));
        //Any consecutive call: value is "two" (last stubbing wins).
        assertEquals("two", mockedList.get(0));
    }

    // 13. Spying on real objects
    @Test
    public void spyingOnRealObject() throws Exception {
        List list = new LinkedList();
        // When you use the spy then the real methods are called (unless a method was stubbed).
        List spy = spy(list);

        //optionally, you can stub out some methods:
        when(spy.size()).thenReturn(100);

        //using the spy calls *real* methods
        spy.add("one");
        spy.add("two");

        //prints "one" - the first element of a list
        assertEquals("one", spy.get(0));

        //size() method was stubbed - 100 is printed
        assertEquals(100, spy.size());

        //optionally, you can verify
        verify(spy).add("one");
        verify(spy).add("two");
        verify(spy).size();

        // Important gotcha on spying real objects!
        // Sometimes it's impossible or impractical to use when(Object) for stubbing spies.
        // Therefore when using spies please consider doReturn|Answer|Throw() family of methods for stubbing. Example:
        List list2 = new LinkedList();
        List spy2 = spy(list2);

        //Impossible: real method is called so spy.get(0) throws IndexOutOfBoundsException (the list is yet empty)
        //when(spy2.get(0)).thenReturn("foo");

        //You have to use doReturn() for stubbing
        doReturn("foo").when(spy2).get(0);
        assertEquals("foo", spy2.get(0));
    }

    // 14. Changing default return values of unstubbed invocations (Since 1.7)

    @Test
    public void unstubbedInvocations() throws Exception {
        //  Unstubbed methods often return null. If your code uses the object returned by an unstubbed call you get a NullPointerException.
        // This implementation of Answer returns SmartNull instead of null. SmartNull gives nicer exception message than NPE
        // because it points out the line where unstubbed method was called. You just click on the stack trace.
        List mockedList = mock(List.class, RETURNS_SMART_NULLS);
        String str = mockedList.toString();
        String str2 = str.toLowerCase();
        assertEquals("NPE", str2);
    }

    // 15. Capturing arguments for further assertions (Since 1.8.0)
    // http://stackoverflow.com/questions/12295891/how-to-use-argumentcaptor-for-stubbing

    @Test
    public void shouldReturnTheSameValue() {
        List mock = mock(List.class);
        when(mock.get(anyInt())).thenAnswer(new Answer() {
            public Object answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                //Object mock = invocation.getMock();
                return "called with arguments: " + args;
            }
        });
    }
}
