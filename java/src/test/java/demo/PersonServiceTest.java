package demo;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 * Created by vhphat on 12/25/2014.
 */
@RunWith(MockitoJUnitRunner.class)
public class PersonServiceTest {
    @Mock PersonDao personDAO;
    @InjectMocks PersonService personService;

    @Test
    public void shouldUpdatePersonName() {
        Person person = new Person(1, "Phillip");
        when(personDAO.fetchPerson( 1 )).thenReturn(person);

        assertTrue(personService.update(1, "David"));
        // optional
        verify(personDAO).fetchPerson(1);

        ArgumentCaptor<Person> personCaptor = ArgumentCaptor.forClass( Person.class );
        verify( personDAO ).update( personCaptor.capture() );

        assertEquals( "David", personCaptor.getValue().getPersonName() );
        // asserts that during the test, there are no other calls to the mock object.
        // verifyNoMoreInteractions( personDAO );
    }

    @Test
    public void shouldNotUpdateIfPersonNotFound() {
        when(personDAO.fetchPerson(1)).thenReturn(null);

        assertFalse(personService.update(1, "David"));

        verify(personDAO).fetchPerson(1);

        verifyZeroInteractions(personDAO);
        verifyNoMoreInteractions(personDAO);
    }
}
